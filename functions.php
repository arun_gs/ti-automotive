<?php

require_once (dirname(__FILE__).'/import-export-products.php');

function my_theme_enqueue_styles() {

    $parent_style = 'parent-style'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );

    wp_enqueue_script( 'jevelin-child-scripts', get_stylesheet_directory_uri() . '/js/scripts.js', array( 'jquery' ) );
    wp_localize_script( 'jevelin-child-scripts', 'my_ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );

add_action('wp_head', 'wpb_add_googleanalytics');
function wpb_add_googleanalytics() { ?>



<?php }

/**
 * Customize product data tabs
 */
add_filter( 'woocommerce_product_tabs', 'woo_custom_description_tab', 98 );
function woo_custom_description_tab( $tabs ) {

	$tabs['description']['callback'] = 'woo_custom_description_tab_content';	// Custom description callback

	return $tabs;
}

function woo_custom_description_tab_content() {
    global $post;
    $horsepower = get_field( "horsepower", $post->ID );
    $flow_rate = get_field( "flow_rate", $post->ID );
    $max_pressure = get_field( "max_pressure", $post->ID );
    $fuel_type = get_field( "fuel_type", $post->ID );
    $pump_body_size = get_field( "pump_body_size", $post->ID );
    $filter_group = get_field( "filter_group", $post->ID );
    $notes = get_field( "notes", $post->ID );

    if($fuel_type)
    echo '<p><strong>Fuel:</strong> '.$fuel_type.'</p>';

    if($horsepower)
    echo '<p><strong>Horsepower:</strong> '.$horsepower.'</p>';

    if($flow_rate) {
        echo '<p><strong>Flow @ 276 kPa:</strong> '.$flow_rate.' lph</p>';
        $flow_rate_gph = round($flow_rate/3.785);
        echo '<p><strong>Flow @ 40 psi:</strong> '.$flow_rate_gph.' gph</p>';
    }

    if($max_pressure) {
        echo '<p><strong>Max System Pressure (psi):</strong> '.$max_pressure.'</p>';
        $max_pressure_kpa = round($max_pressure*6.895);
        echo '<p><strong>Max System Pressure (kPa):</strong> '.$max_pressure_kpa.'</p>';
    }

    if($filter_group) {
        echo '<p><strong>Filter Group:</strong> '.$filter_group.'</p>';
    }

    if($pump_body_size)
    echo '<p><strong>Inlet Diameter:</strong> '.$pump_body_size.'</p>';

    if($post->post_content)
    echo '<p><strong>Inlet Configuration:</strong> '.$post->post_content.'</p>';

    if($post->notes)
    echo '<p>'.$post->notes.'</p>';

    $url_horsepower = htmlspecialchars($_GET["horsepower"]);
    $url_flow_rate = htmlspecialchars($_GET["flow_rate"]);
    $url_max_pressure = htmlspecialchars($_GET["max_pressure"]);
    $url_fuel_type = htmlspecialchars($_GET["fuel_type"]);


    $searchparams = '?horsepower='.$url_horsepower.'&flow_rate='.$url_flow_rate.'&max_pressure='.$url_max_pressure.'&fuel_type='.$url_fuel_type;

    echo '<a href="/interactive-filter/'.$searchparams.'" class="btn-main results-btn back-to-search">BACK TO SEARCH RESULTS</a>';
}

/** 
 * Ajax Call for filtering universal high performance pumps
**/

add_action( 'wp_ajax_uhp_search_products', 'uhp_search_products_callback' );
// If you want not logged in users to be allowed to use this function as well, register it again with this function:
add_action( 'wp_ajax_nopriv_uhp_search_products', 'uhp_search_products_callback' );

function uhp_search_products_callback() {

    $horsepower = trim($_POST['horsepower']);
    $flow_rate = trim($_POST['flow_rate']);
    $max_pressure = trim($_POST['max_pressure']);
    $fuel_type = trim($_POST['fuel_type']);

    $searchparams = '?horsepower='.$horsepower.'&flow_rate='.$flow_rate.'&max_pressure='.$max_pressure.'&fuel_type='.$fuel_type;

    $productsTemplateKits = '';
    $productsTemplatePumps = '';

    $query_args = array( 'post_type' => 'product', 'tax_query' => array(), 'meta_query' => array(), 'posts_per_page' => -1 );

    if ( !empty( $horsepower ) ) {
        $query_args['meta_query'][] = array(
            array(
                'key' => 'horsepower',
                'value' => $horsepower
            )
        );
    }

    if ( !empty( $flow_rate ) ) {
        $query_args['meta_query'][] = array(
            array(
                'key' => 'flow_rate',
                'value' => $flow_rate
            )
        );
    }

    if ( !empty( $max_pressure ) ) {
        $query_args['meta_query'][] = array(
            array(
                'key' => 'max_pressure',
                'value' => $max_pressure
            )
        );
    }

    if ( !empty( $fuel_type ) ) {
        $query_args['meta_query'][] = array(
            array(
                'key' => 'fuel_type',
                'value' => $fuel_type
            )
        );
    }

    $query_args['tax_query'][] = array(
        array(
            'taxonomy' => 'product_cat',
            'field' => 'id',
            'terms' => 54
        )
    );

    $products = get_posts( $query_args );
    $totalcount = count($products);

    foreach ($products as $product) {  

        $horsepower = get_field( "horsepower", $product->ID );
        $flow_rate = get_field( "flow_rate", $product->ID );
        $max_pressure = get_field( "max_pressure", $product->ID );
        $fuel_type = get_field( "fuel_type", $product->ID );
        $pump_type = get_field( "pump_type", $product->ID );

        $featured_img_url = get_the_post_thumbnail_url($product->ID, 'full');
        if(!$featured_img_url) $featured_img_url = '/wp-content/plugins/woocommerce/assets/images/placeholder.png';

        if($pump_type == 'Dual In-Tank Pump Kit' || $pump_type == 'In-Tank Pump Kit' || $pump_type == 'In-Line Pump Kit') {

            $productsTemplateKits .= '<div class="list-item">';
                $productsTemplateKits .= '<div class="img-wrapper">';
                    $productsTemplateKits .= '<a href="'.esc_url( get_permalink($product->ID)).$searchparams.'">';
                    $productsTemplateKits .= '<img src="'.$featured_img_url.'" alt="TI">';
                    $productsTemplateKits .= '</a>';
                $productsTemplateKits .= '</div>';
                $productsTemplateKits .= '<div class="details-wrapper">';
                    $productsTemplateKits .= '<span class="item-code">'.$product->post_title.'</span>';
                    $productsTemplateKits .= '<a href="'.esc_url( get_permalink($product->ID)).$searchparams.'" class="read-more">Read More</a>';
                $productsTemplateKits .= '</div>';
            $productsTemplateKits .= '</div>';

        } else {

            $productsTemplatePumps .= '<div class="list-item">';
                $productsTemplatePumps .= '<div class="img-wrapper">';
                    $productsTemplatePumps .= '<a href="'.esc_url( get_permalink($product->ID)).$searchparams.'">';
                    $productsTemplatePumps .= '<img src="'.$featured_img_url.'" alt="TI">';
                    $productsTemplatePumps .= '</a>';
                $productsTemplatePumps .= '</div>';
                $productsTemplatePumps .= '<div class="details-wrapper">';
                    $productsTemplatePumps .= '<span class="item-code">'.$product->post_title.'</span>';
                    $productsTemplatePumps .= '<a href="'.esc_url( get_permalink($product->ID)).$searchparams.'" class="read-more">Read More</a>';
                $productsTemplatePumps .= '</div>';
            $productsTemplatePumps .= '</div>';

        }

    }

    if ( !$products ) {
        $response['success'] = 'false';
        $response['result'] =  'No posts found';
    } else {
        $response['success'] = 'true';
        $response['resultkits'] = $productsTemplateKits;
        $response['resultpumps'] = $productsTemplatePumps;
        $response['totalcount'] = $totalcount;
    }
    wp_send_json( $response );
}

/** 
 * Ajax Call for fetching filter values based on selection.
**/

add_action( 'wp_ajax_check_available_filters', 'check_available_filters_callback' );
// If you want not logged in users to be allowed to use this function as well, register it again with this function:
add_action( 'wp_ajax_nopriv_check_available_filters', 'check_available_filters_callback' );

function check_available_filters_callback() {

    $horsepower     = trim($_POST['horsepower']);
    $flow_rate      = trim($_POST['flow_rate']);
    $max_pressure   = trim($_POST['max_pressure']);
    $fuel_type      = strtoupper(trim($_POST['fuel_type']));
    $sliderType     = trim($_POST['slider']);

    $query_args = array( 'post_type' => 'product', 'tax_query' => array(), 'meta_query' => array(), 'posts_per_page' => -1 );

    if ( !empty( $horsepower ) ) {
        $query_args['meta_query'][] = array(
            array(
                'key' => 'horsepower',
                'value' => $horsepower
            )
        );
    }

    if ( !empty( $flow_rate ) ) {
        $query_args['meta_query'][] = array(
            array(
                'key' => 'flow_rate',
                'value' => $flow_rate
            )
        );
    }

    if ( !empty( $max_pressure ) ) {
        $query_args['meta_query'][] = array(
            array(
                'key' => 'max_pressure',
                'value' => $max_pressure
            )
        );
    }

    if ( !empty( $fuel_type ) ) {
        $query_args['meta_query'][] = array(
            array(
                'key' => 'fuel_type',
                'value' => $fuel_type
            )
        );
    }

    $query_args['tax_query'][] = array(
        array(
            'taxonomy' => 'product_cat',
            'field' => 'id',
            'terms' => 54
        )
    );

    $products = get_posts( $query_args );

    $results = [];
    $hp_arr = [];
    $fr_arr = [];
    $mp_arr = [];
    $ft_arr = [];

    foreach ($products as $product) {  

        $horsepower = get_field( "horsepower", $product->ID );
        if(!in_array($horsepower, $hp_arr)){
            $hp_arr[]=$horsepower;
        }

        $flow_rate = get_field( "flow_rate", $product->ID );
        if(!in_array($flow_rate, $fr_arr)){
            $fr_arr[]=$flow_rate;
        }

        $max_pressure = get_field( "max_pressure", $product->ID );
        if(!in_array($max_pressure, $mp_arr)){
            $mp_arr[]=$max_pressure;
        }

        $fuel_type = get_field( "fuel_type", $product->ID );
        if(!in_array($fuel_type, $ft_arr)){
            $ft_arr[]=$fuel_type;
        }

    }

    $results['horsepower']      = $hp_arr;
    $results['flow_rate']       = $fr_arr;
    $results['max_pressure']    = $mp_arr;
    $results['fuel_type']       = $ft_arr;
    $results['sliderType']      = $sliderType;

    if ( !$products ) {
        $response['success'] = 'false';
        $response['results'] =  $results;
    } else {
        $response['success'] = 'true';
        $response['results'] = $results;
    }
    wp_send_json( $response );
}

/** 
 * Hide default WP Editor for page-interactive-filter.php page template
**/

add_action( 'admin_init', 'hide_editor' );
 
function hide_editor() {
    $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
    if( !isset( $post_id ) ) return;
 
    $template_file = get_post_meta($post_id, '_wp_page_template', true);
     
    if($template_file == 'page-interactive-filter.php') {
        remove_post_type_support('page', 'editor');
    }
}