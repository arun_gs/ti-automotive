jQuery(document).ready(function ($) {     
    detectIE();
    function detectIE() {
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf('MSIE ');
        var trident = ua.indexOf('Trident/');
        var edge = ua.indexOf('Edge/');
        if (msie > 0) {
            jQuery("body").addClass("ie");
        }
        else if (trident > 0) {
            jQuery('body').addClass("ie ie-11");
        }
        else if (edge > 0) {
            jQuery("body").addClass("ie-edge");
        }
        return false;
    }
    // Global range values
    var hp_values   = [0, 300, 350, 400, 420, 450, 500, 550, 650, 850, 985, 1000, 1250, 1450, 1600];
    var fr_values   = [0, 190, 255, 340, 350, 400, 450, 470, 490, 500, 550, 725, 775];
    var mp_values   = [0, 50, 87, 112];
    var fuel_values = [0, 1, 2];
    // Initial slider setting.
    initialSliderSet('init'); 
    /*
    * Init, Reset Function
    * @Author Arun George
    */
    function initialSliderSet(actType){
        $('.range-input').each(function (e) {
            if (actType == 'init'){
                var min     = $(this).attr('min'),
                max         = $(this).attr('max'),
                val         = $(this).val(),
                sliderid    = $(this).attr('id');
            } else if (actType == 'reset'){
                var min     = $(this).attr('min'),
                max         = $(this).attr('max'),
                val         = 0,
                sliderid    = $(this).attr('id');
            }
            $('#horsepower_range_value').val('');
            $('#flowrate_range_value').val('');
            $('#maxpressure_range_value').val('');
            $("#" + sliderid).css({
                'backgroundSize': (val - min) * 100 / (max - min) + '% 100%'
            });
            // Set Meter
            setValue(val, sliderid+'-meter');
            if (sliderid == 'horsepower'){
                var range = hp_values;
            } else if (sliderid == 'flow-rate') {
                var range = fr_values;
            } else if (sliderid == 'max-pressure') {
                var range = mp_values;
            } else if (sliderid == 'fuel-type') {
                var range = fuel_values;
            }     
            if (actType == 'reset') {
                $('#' + sliderid).val(range.indexOf(parseInt(range[val])));     
            }
            $(this).closest(".range-inner").find(".value, .range-counter").val(range[val]).text((range[val])).trigger('input');
            if (sliderid == 'fuel-type') {
                if (val == min) {
                    $('#fuel-type').val(-1);
                    $(this).closest(".toggle-wrapper").removeClass("max").addClass("min");
                    $(".fuel-meter").removeClass("max").addClass("min");
                    $(this).closest(".range-inner").find(".value").val("gas").trigger('input');
                } else if (val == max) {
                    $('#fuel-type').val(1);
                    $(this).closest(".toggle-wrapper").removeClass("min").addClass("max");
                    $(".fuel-meter").removeClass("min").addClass("max");
                    $(this).closest(".range-inner").find(".value").val("flex").trigger('input');
                } else {
                    $('#fuel-type').val(0);
                    $(this).closest(".toggle-wrapper").removeClass("min max");
                    $(".fuel-meter").removeClass("min max");
                    $(this).closest(".range-inner").find(".value").val("").trigger('input');
                }
            }
        });
        jQuery('#filterTrobber').hide();
		var horsepower      = jQuery('#horsepower_value').val();
		var flow_rate       = jQuery('#flow-rate_value').val();
		var max_pressure    = jQuery('#max-pressure_value').val();
		var fuel_type       = jQuery('#fuel-type_value').val().toUpperCase();
        loadFilterPumps(horsepower, flow_rate, max_pressure, fuel_type);
    }
    /*
    * Function to set Meter Value
    * @Author Arun George
    */
    function setValue(_val, meter) {
        var START, delta;
        var mainPoint   = Math.floor(_val / 100);
        var smallPoint  = Math.round(_val / 100);
        var i           = 1;
        switch (meter) {
            case "horsepower-meter":
                deg     = 0;
                START   = 0;
                delta   = 0.0818;
                break;
            case "flow-rate-meter":
                deg     = 0;
                START   = 0;
                delta   = 0.218;
                break;
            case "max-pressure-meter":
                deg     = 0;
                START   = 0;
                delta   = 1.15;
                break;
        }
        $('.' + meter + ' .mainPoints, .' + meter +' .midPoints').find('path').removeClass('active');
        while (i <= mainPoint + 1) {
            $('.' + meter +' .mainPoints').find('path:nth-child(' + i + ')').addClass('active');
            // for small point correction
            if (isNaN(false)) {
                if (smallPoint > mainPoint) {
                    $('.' + meter +' .midPoints').find('path:nth-child(' + (i) + ')').addClass('active');
                }
                else {
                    $('.' + meter +' .midPoints').find('path:nth-child(' + (i - 1) + ')').addClass('active');
                }
            }
            i++;
        }
        // For Current marking
        $('.' + meter + ' .mainPoints, .' + meter +' .midPoints').find('path').removeClass('current');
        if (isNaN(false)) {
            if (smallPoint > mainPoint) {
                $('.' + meter +' .midPoints').find('path:nth-child(' + (mainPoint + 1) + ')').addClass('current');
            }
            else {
                $('.' + meter +' .mainPoints').find('path:nth-child(' + (mainPoint + 1) + ')').addClass('current');
            }
        }         
        $('.' + meter).find('.counter, .counter-ie').text(_val);
        deg = (START + _val) * delta;
            $('.' + meter).find('.arrow').css({
                "transform": 'rotate(' + deg + 'deg)'
            });
        switch ($('.' + meter).find('.counter').text().length) {
            case 1:
                if (meter != "horsepower-meter") {
                    $('.' + meter).find('.counter').attr('x', '-6%');
                } else {
                    $('.' + meter).find('.counter').attr('x', '-5%');
                }
                break;
            case 2:
                if (meter != "horsepower-meter") {
                    $('.' + meter).find('.counter').attr('x', '-13%');
                } else {
                    $('.' + meter).find('.counter').attr('x', '-9%');
                }
                break;
            case 3:
                if (meter != "horsepower-meter") {
                    $('.' + meter).find('.counter').attr('x', '-20%');
                } else {
                    $('.' + meter).find('.counter').attr('x', '-13%');
                }
                break;
            case 4:
                $('.' + meter).find('.counter').attr('x', '-18%');
                break;
        }
    };




    // Trigger each Slider
    $('input[type=range]').on('input change', function (e) {
        e.stopPropagation();
        var min         = e.target.min,
            max         = e.target.max,
            val         = e.target.value,
            sliderid    = e.target.id;

        if (sliderid == 'horsepower') {
            var range       = hp_values;
            var rangeofval  = range[val];
            $('#flow-rate_value').val(0).text(0);
            $('#max-pressure_value').val(0).text(0);
        } else if (sliderid == 'flow-rate') {
            var range       = fr_values;
            var rangeofval  = range[val];
            $('#max-pressure_value').val(0).text(0);
            var flowrate_range_value        = $('#flowrate_range_value').val();
            if (flowrate_range_value != '') {
                var flowrate_range_value    = JSON.parse(flowrate_range_value);
                if (jQuery.inArray(rangeofval, flowrate_range_value) != -1) {
                } else {
                    var closest     = getClosest(flowrate_range_value, rangeofval);
                    rangeofval      = closest;
                    $('#flow-rate').val(range.indexOf(parseInt(closest)));
                    var val         = e.target.value;
                 } 
            }
        } else if (sliderid == 'max-pressure') {
            var range       = mp_values;
            var rangeofval  = range[val];
            $('#fuel-type_value').val(0).text(0);
            var maxpressure_range_value     = $('#maxpressure_range_value').val();
            if (maxpressure_range_value != '') {
                var maxpressure_range_value = JSON.parse(maxpressure_range_value);
                if (jQuery.inArray(rangeofval, maxpressure_range_value) != -1) {
                } else {
                    var closest     = getClosest(maxpressure_range_value, rangeofval);
                    rangeofval      = closest;
                    $('#max-pressure').val(range.indexOf(parseInt(closest)));
                    var val = e.target.value;
                }
            }
        } 
        else if (sliderid == 'fuel-type') {
            var range               = fuel_values;
            var rangeofval          = range[val];
            var fuel_range_value    = $('#fuel_range_value').val();
            if (fuel_range_value != '') {
                var fuel_range_value    = JSON.parse(fuel_range_value);
                if (jQuery.inArray(rangeofval, fuel_range_value) != -1) {

                } else {
                    var closest     = getClosest(fuel_range_value, rangeofval);
                    rangeofval      = closest;
                    $('#fuel-type').val(range.indexOf(parseInt(closest)));
                    var val         = e.target.value;
                }
            }
        } 
        $("#" + sliderid).css({
            'backgroundSize': (val - min) * 100 / (max - min) + '% 100%'
        });
        setValue(rangeofval, sliderid + '-meter');
        var fltype                  = "";
        if (sliderid == 'fuel-type') {
            if (val == min) {
                $(this).closest(".toggle-wrapper").removeClass("max").addClass("min");
                $(".fuel-meter").removeClass("max").addClass("min");
                $(this).closest(".range-inner").find(".value").val("gas").trigger('input');
            } else if (val == max) {
                $(this).closest(".toggle-wrapper").removeClass("min").addClass("max");
                $(".fuel-meter").removeClass("min").addClass("max");
                $(this).closest(".range-inner").find(".value").val("flex").trigger('input');
            } else {
                $(this).closest(".toggle-wrapper").removeClass("min max");
                $(".fuel-meter").removeClass("min max");
                $(this).closest(".range-inner").find(".value").val("0").trigger('input');
            }
            if ($('#fuel-type_value').val() == "") {
                fltype  = "GAS";
            }
            else if ($('#fuel-type_value').val() == "1") {
                fltype  = "FLEX";
            }
            else if ($('#fuel-type_value').val() == "0") {
                fltype  = "";
            }         
        } else{
            $(this).closest(".range-inner").find(".value, .range-counter").val(rangeofval).text(rangeofval).trigger('input');
        }       
        if (val != 0 ){
            var filters = checkAvailableFilters($('#horsepower_value').val(), $('#flow-rate_value').val(), $('#max-pressure_value').val(), fltype, sliderid);
            filtersliders(filters);
        }
    }).trigger('input');
    /*
    * Function for check available filters
    * @Author Arun George
    */
    function checkAvailableFilters(horsepower, flow_rate, max_pressure, fuel_type, sliderid) {
        $('#filterTrobber').show();
        var result;
        $.ajax({
            type: 'POST',
            url: my_ajax_object.ajax_url,
            dataType: 'json',
            data: {
            'action'        : 'check_available_filters',
            'horsepower'    : horsepower,
            'flow_rate'     : flow_rate,
            'max_pressure'  : max_pressure,
            'fuel_type'     : fuel_type,
            'slider'        : sliderid            
            },
                async: false
            })
        .promise()
        .done(function (data) {
            if (data.success) {
                result = data.results;
            }
            jQuery('#filterTrobber').hide();
        });
        return result;
	}
    /*
    * Function to set slider based on filter return
    * @Author Arun George
    */
    function filtersliders(filters){
        if (filters['sliderType'] == 'horsepower') {
            var val         = $('#flow-rate').val();
            $('#flowrate_range_value').val(JSON.stringify(filters['flow_rate']));
            $('#maxpressure_range_value').val(JSON.stringify(filters['max_pressure']));
            $('#fuel_range_value').val(JSON.stringify(filters['fuel_type']));
            $('#flow-rate_value').val(filters['flow_rate'][0]);
            $('#max-pressure_value').val(filters['max_pressure'][0]);
            $('#fuel-type_value').val(filters['fuel_type'][0]);
            setTheRangeValue(fr_values, val, filters['flow_rate'], 'flow-rate');
            var val         = $('#max-pressure').val();
            setTheRangeValue(mp_values, val, filters['max_pressure'], 'max-pressure');
            var val         = $('#fuel-type').val();
            setTheRangeValue(fr_values, val, filters['fuel_type'], 'fuel-type');             
        } else if (filters['sliderType'] == 'flow-rate') {
            $('#maxpressure_range_value').val(JSON.stringify(filters['max_pressure']));
            $('#fuel_range_value').val(JSON.stringify(filters['fuel_type']));
            $('#max-pressure_value').val(filters['max_pressure'][0]);
            $('#fuel-type_value').val(filters['fuel_type'][0]);
            var val         = $('#max-pressure').val();
            setTheRangeValue(mp_values, val, filters['max_pressure'], 'max-pressure');
            var val         = $('#fuel-type').val();
            setTheRangeValue(fr_values, val, filters['fuel_type'], 'fuel-type');
        } else if (filters['sliderType'] == 'max-pressure') {
            $('#fuel_range_value').val(JSON.stringify(filters['fuel_type']));
            $('#fuel-type_value').val(filters['fuel_type'][0]);
            var val         = $('#fuel-type').val();
            setTheRangeValue(fr_values, val, filters['fuel_type'], 'fuel-type');
        } 
        var horsepower      = $('#horsepower_value').val();
        var flow_rate       = $('#flow-rate_value').val();
        var max_pressure    = $('#max-pressure_value').val();
        var fuel_type       = $('#fuel-type_value').val().toUpperCase();
        loadFilterPumps(horsepower, flow_rate, max_pressure, fuel_type);
    }
    /*
    * Function to set the range values to sliders
    * @Author Arun George
    */
    function setTheRangeValue(range, val, returnvalarray, sliderTypeid){
        if (sliderTypeid == 'fuel-type'){
            if (returnvalarray[0] == 'GAS') {
                $('#' + sliderTypeid).val(-1);
                $('#' + sliderTypeid).closest(".toggle-wrapper").removeClass("max").addClass("min");
                $(".fuel-meter").removeClass("max").addClass("min");
                $('#' + sliderTypeid).closest(".range-inner").find(".value").val("gas").trigger('input');
            } else if (returnvalarray[0] == 'FLEX') {
                $('#' + sliderTypeid).val(1);
                $('#' + sliderTypeid).closest(".toggle-wrapper").removeClass("min").addClass("max");
                $(".fuel-meter").removeClass("min").addClass("max");
                $('#' + sliderTypeid).closest(".range-inner").find(".value").val("1").trigger('input');
            } else {
                $('#' + sliderTypeid).val(0);
                $('#' + sliderTypeid).closest(".toggle-wrapper").removeClass("min max");
                $(".fuel-meter").removeClass("min max");
                $('#' + sliderTypeid).closest(".range-inner").find(".value").val("0").trigger('input');
            }
        }else{
            $('.range-inner--' + sliderTypeid).find('.ruler-value').addClass('active');
            $('.range-inner--' + sliderTypeid + ' .ruler-value span').removeClass('avl');
            $('#' + sliderTypeid).val(range.indexOf(parseInt(returnvalarray)));
            $.each(returnvalarray, function (index, value) {
                $('.range-inner--' + sliderTypeid + ' .ruler-value [data-value="' + value + '"]').addClass('avl');
            });
            var slideval    = $('#' + sliderTypeid).val();
            var slidemin    = $('#' + sliderTypeid).attr('min');
            var slidemax    = $('#' + sliderTypeid).attr('max');
            $("#" + sliderTypeid).css({
                'backgroundSize': (slideval - slidemin) * 100 / (slidemax - slidemin) + '% 100%'
            });
        }
        setValue(returnvalarray[0], sliderTypeid + '-meter');
    }
    /*
    * Function to find the Closest value from the array returned
    * @Author Arun George
    */
    function getClosest(arr, val) {
        return arr.reduce(function (prev, curr) {
            return (Math.abs(curr - val) < Math.abs(prev - val) ? curr : prev);
        });
    }
    /*
    * Reset the slider
    */
    jQuery('#reset').on('click',function() {
        initialSliderSet('reset');
    });
    /*
    * Filter ajax fired on each slider selection
    * @Author Arun George
    */ 
    function loadFilterPumps(horsepower, flow_rate, max_pressure, fuel_type) {
        var selected_filters = '';
        if (horsepower && horsepower != 0) {
            selected_filters += '<div class="col"><label for="">Horsepower: </label><span>' + horsepower + '</span></div>';
        }
        if (flow_rate && flow_rate != 0) {
            selected_filters += '<div class="col"><label for="">Flow Rate: </label><span>' + flow_rate + '</span></div>';
        }
        if (max_pressure && max_pressure != 0) {
            selected_filters += '<div class="col"><label for="">Max Pressure: </label><span>' + max_pressure + '</span></div>';
        }
        if (fuel_type) {
            selected_filters += '<div class="col"><label for="">Fuel Type: </label><span>' + fuel_type + '</span></div>';
        }
        if (selected_filters == '') {
            selected_filters = '<div class="col"><label for="">No Filters Selected</label></div>';
        }
        $('#selected_filters .col').remove();
        $('#selected_filters').append(selected_filters);
        $.ajax({
            type: 'POST',
            url: my_ajax_object.ajax_url,
            data: {
                'action'        : 'uhp_search_products',
                'horsepower'    : horsepower,
                'flow_rate'     : flow_rate,
                'max_pressure'  : max_pressure,
                'fuel_type'     : fuel_type
        }
		})
        .promise()
        .done(function (data) {
            if (data.success == 'true') {
                var totalcount = data.totalcount;
                if (totalcount > 1) {
                    $('#tot_count').text(totalcount + ' TOTAL PUMPS');
                    $('#noPumpsFound').hide();
                } else if (totalcount == 1) {
                    $('#tot_count').text(totalcount + ' TOTAL PUMP');
                    $('#noPumpsFound').hide();
                } else {
                    $('#tot_count').text('NO PUMPS FOUND');
                    $('#noPumpsFound').show();
                }
                $('#pumpKitsWrapper .list-item').remove();
                $('#pumpsWrapper .list-item').remove();
                if (data.resultkits) {
                    $('#pumpKitsHeading').show();
                    $('#pumpKitsWrapper').show();
                    $('#pumpKitsWrapper').append(data.resultkits);
                } else {
                    $('#pumpKitsHeading').hide();
                    $('#pumpKitsWrapper').hide();
                }
                if (data.resultpumps) {
                    $('#pumpsHeading').show();
                    $('#pumpsWrapper').show();
                    $('#pumpsWrapper').append(data.resultpumps);
                } else {
                    $('#pumpsHeading').hide();
                    $('#pumpsWrapper').hide();
                }
            } else {
                $('#tot_count').text('NO PUMPS FOUND');
                $('#noPumpsFound').show();
                $('#pumpKitsHeading').hide();
                $('#pumpKitsWrapper').hide();
                $('#pumpsHeading').hide();
                $('#pumpsWrapper').hide();
                $('#pumpKitsWrapper .list-item').remove();
                $('#pumpsWrapper .list-item').remove();
            }
            $('#filterTrobber').hide();
        });
	}
});
