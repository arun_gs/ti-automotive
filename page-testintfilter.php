<?php
/**
 * Template Name: Test Filter
 */

get_header(); ?>

<div id="content" class="page-content">
<!-- HTML GOES HERE -->
	<br>
	<br>
	<br>
	<center>HTML GOES HERE</center>
<!-- HTML GOES HERE -->
<?php get_footer(); ?>
<script>
jQuery('document').ready(function ($) {
	
	var horsepower = 350;
	var flow_rate = 190;
	var max_pressure = 87;
	var fuel_type = 'GAS';

    $.ajax({
        type: 'POST',
        url: '<?php echo admin_url('admin-ajax.php'); ?>',
        data: {
            'horsepower': horsepower,
            'flow_rate': flow_rate,
            'max_pressure': max_pressure,
            'fuel_type': fuel_type,
            'action': 'uhp_search_products' 
        }
    })
    .promise()
    .done( function(data) {
        if(data.success == 'true') {
            $('#content').append('<h2>Fuel Pump Kits</h2>');
            $('#content').append(data.resultkits);
            $('#content').append('<h2>Fuel Pumps</h2>');
            $('#content').append(data.resultpumps);
        } 
    });

});


</script>
