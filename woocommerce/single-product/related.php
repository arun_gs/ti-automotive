<?php
/**
 * Related Products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/related.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if( has_term( 54, 'product_cat' ) ) { 
	
	global $post;
	
	$uhp_relatedproducts = get_field( "related_products", $post->ID );
	
	if ( $uhp_relatedproducts["related_product_1"] || $uhp_relatedproducts["related_product_2"] || $uhp_relatedproducts["related_product_3"] || $uhp_relatedproducts["related_product_4"] ) : ?>

	<section class="related products">

		<h2><?php esc_html_e( 'Other Fuel Pumps and Kits', 'woocommerce' ); ?></h2>

		<?php woocommerce_product_loop_start(); ?>

			<?php 
			
			
			
			foreach ( $uhp_relatedproducts as $related_product ) : ?>

				<?php if($related_product->ID) {
					
					$post_object = get_post( $related_product->ID );

					setup_postdata( $GLOBALS['post'] =& $post_object );

					wc_get_template_part( 'content', 'product' ); 
					
				} ?>

			<?php endforeach; ?>

		<?php woocommerce_product_loop_end(); ?>

	</section>

<?php endif;

} else {

	if ( $related_products ) : ?>

		<section class="related products">

			<h2><?php esc_html_e( 'Other Featured Products', 'woocommerce' ); ?></h2>

			<?php woocommerce_product_loop_start(); ?>

				<?php foreach ( $related_products as $related_product ) : ?>

					<?php
						$post_object = get_post( $related_product->get_id() );

						setup_postdata( $GLOBALS['post'] =& $post_object );

						wc_get_template_part( 'content', 'product' ); ?>

				<?php endforeach; ?>

			<?php woocommerce_product_loop_end(); ?>

		</section>

	<?php endif;

}

wp_reset_postdata();
